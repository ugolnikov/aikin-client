package configuration

import (
	"fmt"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigType("json")
	viper.SetConfigFile("./config/clientConfig.json")
	fmt.Printf("Using config: %s\n", viper.ConfigFileUsed())
	viper.ReadInConfig()
	if viper.IsSet("host") {
		fmt.Println("Using host: ", viper.Get("host"))
	} else {
		fmt.Println("Condiguration key \"host\" need, but is not set!")
	}
}

func GetHost() string {
	return viper.GetString("host")
}
