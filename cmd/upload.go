package cmd

import (
	"aikin-client/configuration"
	"bufio"
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"time"
)

var HOST string = configuration.GetHost()

var uploadCmd = &cobra.Command{
	Use:   "upload",
	Short: "This command uploads current plugin to the server and named it",
	Long:  `This command uploads current plugin to the server and named it`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("upload called")

		pluginName, err := cmd.Flags().GetString("name")
		if err != nil {
			fmt.Println("Error: wrong argument name")
		}
		pluginPath, err := cmd.Flags().GetString("path")
		if err != nil {
			fmt.Println("Error: wrong argument path")
		}

		fmt.Printf("uploading plugin %s from %s to the system\n", pluginName, pluginPath)

		client := &http.Client{
			Timeout: 30 * time.Second,
		}

		file, err := os.Open(pluginPath)
		defer file.Close()
		if err != nil {
			fmt.Println(err)
			return
		}

		req, err := http.NewRequestWithContext(context.Background(),
			http.MethodPost, HOST+"/upload", file)
		if err != nil {
			panic(err)
		}
		req.Header.Add("PluginName", pluginName)

		res, err := client.Do(req)
		if err != nil {
			panic(err)
		}

		var scanner = bufio.NewScanner(res.Body)
		fmt.Printf("Server response: ")
		for scanner.Scan() {
			fmt.Printf("%q\n", scanner.Text())
		}
		err = scanner.Err()
		if err != nil {
			panic(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(uploadCmd)
	uploadCmd.Flags().StringP("name", "n", "", "name for used existing plugin")
	uploadCmd.Flags().StringP("path", "p", "", "path for existing plugin")
}
