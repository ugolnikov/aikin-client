package cmd

import (
	"bufio"
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/spf13/cobra"
)

// todo углубить тему авторизации в системе
var registerCmd = &cobra.Command{
	Use:   "register",
	Short: "Register a session in system",
	Long:  `Register a session in system`,
	Run: func(cmd *cobra.Command, args []string) {
		sessionId, password := args[0], args[1]
		client := &http.Client{
			Timeout: 30 * time.Second,
		}

		req, err := http.NewRequestWithContext(context.Background(),
			http.MethodGet, HOST+"/authorization", nil)
		if err != nil {
			panic(err)
		}
		//todo сделать авторизацию средствами http или при помощи специальных средств авторизации
		req.Header.Add("SessionId", sessionId)
		req.Header.Add("Password", password)

		res, err := client.Do(req)
		if err != nil {
			panic(err)
		}

		var scanner = bufio.NewScanner(res.Body)
		fmt.Printf("Server response: ")
		for scanner.Scan() {
			fmt.Printf("%q\n", scanner.Text())
		}
		err = scanner.Err()
		if err != nil {
			panic(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(registerCmd)
}
