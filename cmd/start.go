package cmd

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"strconv"
	"time"
)

// todo добавить механизм форматирования данных через json (файл <pluginName><taskId>Format.json)
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Start your <name> plugin to calculating with current data.",
	Long:  `Start your <name> plugin to calculating with current data.`,
	Run: func(cmd *cobra.Command, args []string) {
		pluginName, err := cmd.Flags().GetString("name")
		if err != nil {
			fmt.Println("Error: wrong argument name")
		}

		dataPath, err := cmd.Flags().GetString("path")
		if err != nil {
			fmt.Println("Error: wrong argument path")
		}
		if len(dataPath) == 0 {
			fmt.Println("Error: empty path")
			return
		}

		jobsCount, err := cmd.Flags().GetString("jobs")
		if err != nil {
			fmt.Println("Error: wrong argument jobs")
		}
		if len(jobsCount) == 0 {
			fmt.Println("Parameter jobs is undefined. Using 1 job as default")
			jobsCount = "1"
		}
		fmt.Printf("Calculating plugin %s with data %s in process with %s jobs..\n", pluginName, dataPath, jobsCount)

		client := &http.Client{
			Timeout: 30 * time.Second,
		}

		file, err := os.Open(dataPath)
		defer file.Close()
		if err != nil {
			fmt.Println(err)
			return
		}

		req, err := http.NewRequestWithContext(context.Background(),
			http.MethodGet, HOST+"/start", file)
		if err != nil {
			panic(err)
		}
		req.Header.Add("PluginName", pluginName)

		// todo сделать генерацию taskId - идентификатор задания на вычисление
		taskId := 222
		req.Header.Add("TaskId", strconv.Itoa(taskId))
		req.Header.Add("JobsCount", jobsCount)

		res, err := client.Do(req)
		if err != nil {
			panic(err)
		}

		taskName := pluginName + strconv.Itoa(taskId)
		resultFilePath := "./plugins/results/" + taskName + "result"
		resultFile, err := os.Create(resultFilePath)
		defer resultFile.Close()
		if err != nil {
			fmt.Println("Client can not save result: ", err.Error())
			defer resultFile.Close()
			return
		}
		resultFile.ReadFrom(res.Body)
		fmt.Printf("Calculating done in %s\n", resultFilePath)
	},
}

func init() {
	rootCmd.AddCommand(startCmd)
	startCmd.Flags().StringP("name", "n", "", "name used existing plugin")
	startCmd.Flags().StringP("path", "p", "", "path for existing source data")
	startCmd.Flags().StringP("jobs", "j", "", "count of requested jobs")
}
