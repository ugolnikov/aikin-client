package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// statusCmd represents the status command
var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "Use this to get status information",
	Long:  `Use this to get status information about plagins, agent and other resources available to you.`,
	Run: func(cmd *cobra.Command, args []string) {

		pluginName, err := cmd.Flags().GetString("name")
		if err != nil {
			fmt.Println("Error: wrong argument name")
		}
		if len(pluginName) == 0 {
			fmt.Println("Error: empty argument name")
		}

		taskId, err := cmd.Flags().GetString("id")
		if err != nil {
			fmt.Println("Error: wrong argument id")
		}
		if len(taskId) == 0 {
			fmt.Println("Error: empty argument id")
		}

		//todo сделать запрос на сервер для получения статуса по таске
		if len(pluginName) == 0 && len(taskId) == 0 {
			return
		} else {
			fmt.Printf("Just now there is no info about %s%s\n", pluginName, taskId)
		}
	},
}

func init() {
	rootCmd.AddCommand(statusCmd)
	statusCmd.Flags().StringP("name", "n", "", "name of active plugin")
	statusCmd.Flags().StringP("id", "i", "", "identifier of active task")
}
