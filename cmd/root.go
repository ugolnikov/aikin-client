package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "aikin-client",
	Short: "Client for AIKIN system.",
	Long: `Client for AIKIN system, that provide you remote
processing for your C++ plugins with your data.`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

// todo написать коммент для --help
func init() {
}
