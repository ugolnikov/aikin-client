# aikin-client

### Что это такое

CLI приложение для взаимодействия пользователя с системой Aikin.

### Использование

| Команда                                    | Описание                                                  |
|--------------------------------------------|-----------------------------------------------------------|
| 1) aikin register \<sessionId> \<password> | Запросите аутентификацию сессии.                          |
| 2) aikin upload [options]                  | После успешной аутентификации, загрузите свой плагин.     |
| 3) aikin start [options]                   | Загрузите данные, запустите процесс обработки.            |
| 4) aikin status [options]                  | Затем можете проверить статус доступных ресурсов системы. |

### CLI Интерфейс 
* **register** <sessionId> <password>
  * sessionId - идентификатор сессии
  * password - пароль для доступа к сессии
* **start** [options]
  * --name -n - идентификатор актуального плагина в системе
  * --path -p - путь к файлам данных
  * --jobs -j - запрос использования конкретного количества агентов
* **status** [options]
  * --name -n - идентификатор актуального плагина в системе
  * --id -i   - идентификатор активной задачи в системе
* **upload*** [options]
  * --name -n - идентификатор плагина для загрузки в систему
  * --path -p - путь к файлу плагина для загрузки в систему