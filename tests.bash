#!/bin/bash
echo Start of testing aikin system

echo
echo
echo Test:  go run ./main.go register testnameA password123
go run ./main.go register testnameA password123

echo
echo
echo Test: go run ./main.go upload testname ./plugins/plugin
go run ./main.go upload -n testname -p ./plugins/plugin

echo
echo
echo Test:  go run ./main.go start -n testname -p ./plugins/plugin
go run ./main.go start -n testname -p ./plugins/data/testname

echo
echo
echo Test:  go run ./main.go status
go run ./main.go status -n testname -id "111"

echo
echo
echo End of tests
